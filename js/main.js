// Accordion slider
(function($) {

	if ($('.accordion').length){

		var	_cfg = {
			accordion: $( ".accordion" ),
			panel: $( ".panel" ),
			panels: [],
			activePanel: $( ".accordion div.panel:first" ),
			panelContent: $( ".accordion .panel .panelContent" ),
			shortPanel: $( ".img_short" ),
			panelHeight: 1200 / 540, // 1200px max width, 540px max height
			contentFirst: $( ".content-first" ),
			contentFirstHeight: 1200 / 610
		};


		var _Accordion = {

			init: function() {
				
				_cfg.panel.each(function() {

					var img = $(this).data('image');
					_cfg.panels.push( img );

					// Set background images for panel content and short panel
					$(this).find('.panelContent').css({ 
						"background": "url(" + img + ") no-repeat",
						"background-size": "100% 100%"
					});
					
					$(this).find('.img_short').css({ 
						"background": "url(" + img + ") no-repeat -100px 0",
						"background-size": "500% 100%"
					});

				});

				var panelNum = _cfg.panels.length;

				_Accordion.resizeAccordion( panelNum );
			},

			resizeAccordion: function( panelNum ) {			

				var viewPortWidth = $(window).width(),
					activePanelWidth = 73 + "%",
					shortPanelWidth = 27 / (panelNum - 1) + "%", // 100% - 73% (active panel) = 27% / total panel num - active pane
					total = viewPortWidth / _cfg.panelHeight,
					totalCf = viewPortWidth / _cfg.contentFirstHeight;

				function setWidth( viewPortWidth ) {
					if ( viewPortWidth <= 1200 && viewPortWidth > 700 ) {
						activePanelWidth = 73 + "%";
						shortPanelWidth = 27 / (panelNum - 1) + "%";					
						_Accordion.mainAccordion( activePanelWidth, shortPanelWidth );
					} else if ( viewPortWidth <= 700 && viewPortWidth > 580 ) {
						activePanelWidth = 67 + "%";
						shortPanelWidth = 33 / (panelNum - 1) + "%";
						_Accordion.mainAccordion( activePanelWidth, shortPanelWidth );
					} else if ( viewPortWidth <= 580 ) {
						activePanelWidth = 61 + "%";
						shortPanelWidth = 39 / (panelNum - 1) + "%";
						_Accordion.mainAccordion( activePanelWidth, shortPanelWidth );
					}
					_cfg.panel.css( "width", shortPanelWidth );
					$( _cfg.activePanel ).css( "width", activePanelWidth );
				}

				function setHeight( viewPortWidth ) {
					total = viewPortWidth / _cfg.panelHeight;
					totalCf = viewPortWidth / _cfg.contentFirstHeight;

					if ( viewPortWidth > 1200 ) {
						_cfg.accordion.height( 540 );
						_cfg.panel.height( 540 );
						_cfg.panelContent.height( 540 );
						_cfg.shortPanel.height( 540 );
						_cfg.contentFirst.height( 610 );
					} else if ( viewPortWidth <= 1200 ) {
						_cfg.accordion.height( total );
						_cfg.panel.height( total );
						_cfg.panelContent.height( total );
						_cfg.shortPanel.height( total );
						_cfg.contentFirst.height( totalCf );
					}
				}

				setWidth( viewPortWidth );
				setHeight( viewPortWidth );				

				$( window ).resize( function() {
					viewPortWidth = $(window).width();
							
					setHeight( viewPortWidth );
					setWidth( viewPortWidth );
				});

				_Accordion.mainAccordion( activePanelWidth, shortPanelWidth );
			},

			mainAccordion: function( activePanelWidth, shortPanelWidth ) {
				
				$( _cfg.activePanel ).addClass( 'active' );				

				_cfg.accordion.delegate('.panel', 'click', function(e) {
					_cfg.panel.css( "width", shortPanelWidth );
					$( _cfg.activePanel ).css( "width", activePanelWidth );
					if( ! $(this).is( '.active' ) ){
						$( _cfg.activePanel ).css( {width: shortPanelWidth}, {duration:400, queue:false} );
						$(this).css( {width: activePanelWidth}, {duration:400, queue:false} );
						$( '.accordion .panel' ).removeClass( 'active' );
						$(this).addClass( 'active' );
						_cfg.activePanel = this;
					};
				});

			}

		};

		$(function(){
			_Accordion.init();
		});

	}

})(jQuery);